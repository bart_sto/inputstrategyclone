import java.util.Scanner;

public class StrategyStdIn implements IInputStrategy{

    private Scanner sc;

    public StrategyStdIn() {
        this.sc = new Scanner(System.in);
    }


    @Override
    public int getInt() {
        return sc.nextInt();
    }

    @Override
    public double getDouble() {
        return sc.nextDouble();
    }

    @Override
    public String getString() {
        return sc.next();
    }
}
