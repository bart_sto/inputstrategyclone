import java.io.File;
import java.util.Random;

public class StrategyRandom implements IInputStrategy {
    private Random rd;

    public StrategyRandom(Random rd) {
        this.rd = new Random();
    }

    @Override
    public File getInt() {
        return rd.nextInt();
    }

    @Override
    public String getString() {

        StringBuilder sb = new StringBuilder();
        int length = rd.nextInt(100);
        for (int i = 0; i <length ; i++) {
            //generatind random character with ascii from 97 ..
            sb.append((char) rd.nextInt(32)+97);

        }
        return sb.toString();
    }

    @Override
    public File getDouble() {
        return rd.nextDouble();
    }
}

